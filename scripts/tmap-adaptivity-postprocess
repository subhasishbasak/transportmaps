#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys
import os
import logging
import TransportMaps as TM
from TransportMaps.CLI import AdaptivityPostprocess

sys.path.append(os.getcwd())

argv = sys.argv[1:]
script = AdaptivityPostprocess(argv)

logging.basicConfig(level=script.LOGGING_LEVEL)
TM.setLogLevel(level=script.LOGGING_LEVEL)

script.load()

mpi_pool = None
if script.NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(script.NPROCS)
    # mpi_pool.set_log_level(10)
    
try:
    script.run(mpi_pool)
finally:
    if mpi_pool is not None:
        mpi_pool.stop()
    if script.INTERACTIVE:
        from IPython import embed
        embed()
    if not script.INTERACTIVE:
        if script.h5_file is not None:
            script.h5_file.close()

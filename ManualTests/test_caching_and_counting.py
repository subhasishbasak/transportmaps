import TransportMaps as TM
import TransportMaps.Distributions as DIST
import numpy as np;
import numpy.random as npr;

# tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(2, 3)
tm = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(2, 3)
tm.coeffs = tm.get_identity_coeffs()
pi = DIST.BananaDistribution(1., 3., np.zeros(2), np.eye(2))
pull_pi = DIST.PullBackTransportMapDistribution(tm, pi)
inp = npr.randn(10,2);

vals1 = pull_pi.log_pdf(inp)
print("After log_pdf")
print(pull_pi.get_ncalls_tree())
print("Reset counters")
pull_pi.reset_counters()
print(pull_pi.get_ncalls_tree())

# Caching
cache = {'tot_size': 10}
# cache = {'evaluate': (np.zeros(10, dtype=bool), np.zeros(10))}
vals2 = pull_pi.log_pdf(inp, cache=cache)
print("After log_pdf (cache storing)")
print(pull_pi.get_ncalls_tree())
# Recompute using cache
vals3 = pull_pi.log_pdf(inp, idxs_slice=slice(3,8), cache=cache)
print("After log_pdf (cache loading)")
print(pull_pi.get_ncalls_tree())

print("Reset counters")
pull_pi.reset_counters()
cache = {'tot_size': 10}
print(pull_pi.get_ncalls_tree())

# Gradient caching
ga1 = pull_pi.grad_a_log_pdf(inp, cache=cache)
print("After grad_a_log_pdf (cache storing)")
print(pull_pi.get_ncalls_tree())
# Recompute using cache
ga2 = pull_pi.grad_a_log_pdf(inp, cache=cache)
print("After grad_a_log_pdf (cache loading)")
print(pull_pi.get_ncalls_tree())

print("Reset counters")
pull_pi.reset_counters()
cache = {'tot_size': 10}
print(pull_pi.get_ncalls_tree())

# Hessian caching
ha1 = pull_pi.hess_a_log_pdf(inp, cache=cache)
print("After hess_a_log_pdf (cache storing)")
print(pull_pi.get_ncalls_tree())
# Recompute using cache
ha2 = pull_pi.hess_a_log_pdf(inp, cache=cache)
print("After hess_a_log_pdf (cache loading)")
print(pull_pi.get_ncalls_tree())


print("Reset counters")
pull_pi.reset_counters()
cache = {'tot_size': 10}
print(pull_pi.get_ncalls_tree())

# Gradient caching
ha1 = pull_pi.grad_a_log_pdf(inp, cache=cache)
print("After grad_a_log_pdf (cache storing)")
print(pull_pi.get_ncalls_tree())
# Recompute using cache
ha2 = pull_pi.hess_a_log_pdf(inp, cache=cache)
print("After hess_a_log_pdf (cache loading)")
print(pull_pi.get_ncalls_tree())


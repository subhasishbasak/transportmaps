CPP-Module Example
==================

This example shows how to integrate existing C++ code to the transport-map framework.


Description
-----------

We are interested approximate a density from samples, but these samples come from a complex C++ code which we don't want to bother to rewrite in Python (which wouldn't make sense from the performance perspective either).

In the interest of the simplicity of the example we actually are going to generate samples from a Weibull(a=2,b=4) distribution, using the implementation provided in the C++ standard library: http://www.cplusplus.com/reference/random/weibull_distribution/


Usage
-----

1) The Python wrapper for the C++ function is provided by the "xun" module, which can be accessed from this directory. One can install the xun module using the command:

 $ cd xun
 $ python setup.py develop
 $ cd ..

Note: I suggest to use the "develop" command rather than the "install" command because this allows one to re-install the same package multiple times, without the need to increase its version (which is very convenient during the development).

2) In the current directory we are now ready to run the main script:

 $ python -i KLdivergence-InverseMap-InverseConstruction.py


Preparing your wrapper
----------------------

The main files that one should modify in order to get his/her own wrapper are:

xun/setup.py    : 
One can provide here additional flags and dependent packages (if any)

xun/src/xun.cpp : 
The actual wrapper. It is fairly documented and there are two additional functions defined ("sum_const_vec", "sum_const_mat") in order to describe the passage of array and matrices (3d array work the similarly).
Note: in order not to mess to much with memory the output vectors are allocated first as PyArrayObject, then one access them as C array through the PyArray_AsCArray. I personally discourage returning PyArrayObjects which are constructed "on top" of C arrays, because then one has to bother with contiguity stuff.



Linking to legacy C/C++/Fortran code
====================================

There is a number of ways to link existing legacy code to python.
The best guidlines toward this goal are provided by the SciPy project:

* `Using Python as glue <https://docs.scipy.org/doc/numpy/user/c-info.python-as-glue.html>`_

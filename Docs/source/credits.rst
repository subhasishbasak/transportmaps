Credits
=======

This sofware has been developed and is being maintained by the `Uncertainty Quantification Group <http://uqgroup.mit.edu>`_ at `MIT <http://mit.edu>`_, under the guidance of Prof. Youssef Marzouk. 

**Developing team**

| Daniele Bigoni -- [|www_dabi|]  
| Alessio Spantini  
| Rebecca Morrison  
| Ricardo M. Baptista
|

**Key contributors to the field of transport maps**

| Youssef Marzouk -- [|www_ymarz|]
| Tarek El Moselhy
| Matthew Parno
| Xun Huan
| Benjamin Zhang
| Zheng Wang
|

**Support**: use the dedicated `q&a web-site <https://transportmaps.mit.edu/qa>`_.  

**Bug-tracking**: report issues on `bitbucket <https://bitbucket.org/dabi86/transportmaps>`_.

.. |www_dabi| raw:: html

   <a href="http://www.limitcycle.it/" target="_blank">www</a>

.. |www_ymarz| raw:: html

   <a href="http://uqgroup.mit.edu/" target="_blank">www</a>

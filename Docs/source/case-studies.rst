Case Studies
============

In the following we report published results and additional case studies, with
their respective configurations and data sets in the interest of reproducibility.

.. toctree::
   :maxdepth: 1

   cs/stochastic-volatility/cs-stochastic-volatility

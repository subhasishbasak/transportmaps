{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "import h5py\n",
    "import dill\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy.stats as stats\n",
    "import SpectralToolbox.Spectral1D as S1D\n",
    "import TransportMaps as TM\n",
    "import TransportMaps.Functionals as FUNC\n",
    "import TransportMaps.Maps as MAPS\n",
    "import TransportMaps.Distributions as DIST\n",
    "%matplotlib inline\n",
    "TM.setLogLevel(logging.INFO)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sparse transports for Markov random fields\n",
    "\n",
    "Markov properties of the target distribution $\\nu_\\pi$ can be exploited in order to derive efficient low-dimensional approximations [[TR4]](aa-references.html#spantini2017). We alreadi discussed ([here](example-sequential-stocvol-6d.html)) the decomposability of direct transports $T$. Here we present explore the sparsity of the inverse transport $S$.\n",
    "\n",
    "We use again the stochastic volatility model as an example. See [[OR7]](aa-references.html#Hull1987) and [[OR8]](aa-references.html#Kim1998) for a presentation of the problem. We use the solution to the [sequential inference problem](example-sequential-stocvol-6d.html) to create a synthetic example, i.e. to generate a sample for which we will assume not to know its origin.\n",
    "\n",
    "Let us load such a sample (we use a $10^4$ long Markov chain from $\\nu_\\pi$) ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "f = h5py.File(\"aux/inv-stocvol/Sequential-IntSq-Postprocess.dill.hdf5\")\n",
    "x = f['metropolis-independent-proposal-samples']['x'][:]\n",
    "w = np.ones(x.shape[0]) / float(x.shape[0])\n",
    "f.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us then define the distribution $\\nu_\\pi$ ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class StocVol(DIST.Distribution):\n",
    "    def __init__(self, x):\n",
    "        super(StocVol,self).__init__(x.shape[1])\n",
    "        self.x = x\n",
    "    def quadrature(self, qtype, qparams, *args, **kwargs):\n",
    "        if qtype == 1: # We use 1 to indicate a Markov chain\n",
    "            nmax = self.x.shape[0]\n",
    "            if qparams > nmax:\n",
    "                raise ValueError(\"Maximum sample size (%d) exceeded\" % nmax)\n",
    "            x = self.x[:qparams,:]\n",
    "            w = np.ones(qparams)/float(qparams)\n",
    "        else: raise ValueError(\"Quadrature not defined\")\n",
    "        return (x,w)\n",
    "pi = StocVol(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now define a simple linear transformation that rescales the position of the sample to the hyper-cube $[-4,4]$ (as in the [introductory example](example-inverse-gumbel-1d.html)) ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a = np.zeros(pi.dim)\n",
    "b = np.zeros(pi.dim)\n",
    "for d in range(pi.dim):\n",
    "    xmax = np.max(x[:,d])\n",
    "    xmin = np.min(x[:,d])\n",
    "    a[d] = 4*(xmin+xmax)/(xmin-xmax)\n",
    "    b[d] = 8./(xmax-xmin)\n",
    "L = MAPS.FrozenLinearDiagonalTransportMap(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sparse inverse transports\n",
    "\n",
    "We know by construction that the stochastic volatility model will have the following Markov structure:\n",
    "\n",
    "![](Figures/SequentialHiddenMarkovModel.png)\n",
    "\n",
    "where $\\Theta=(\\mu,\\phi)$ and ${\\bf Z}_k \\in \\mathbb{R}$. Using the theory on sparse inverse transports [[TR4]](aa-references.html#spantini2017) we then expect $S$ to have the following sparsity structure:\n",
    "\n",
    "![](Figures/inv-sparsity.svg)\n",
    "\n",
    "Let us then assemble the map $S$ of total order 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of coefficients: 64\n"
     ]
    }
   ],
   "source": [
    "order = 2\n",
    "active_vars = [[0], [0,1], [0,1,2], [0,1,2,3],\n",
    "               [0,1,3,4], [0,1,4,5]]\n",
    "S = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(\n",
    "    6, order, 'total', active_vars=active_vars)\n",
    "print(\"Number of coefficients: %d\" % S.n_coeffs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variational solution\n",
    "\n",
    "We can now set up the problem\n",
    "\n",
    "$$\n",
    "\\hat{S} = \\arg\\min_{S\\in\\mathcal{T}_\\triangle} \\mathcal{D}_{\\rm KL}\n",
    "\\left( S_\\sharp L_\\sharp \\nu_\\pi \\middle\\Vert \\nu_\\rho \\right) \\;,\n",
    "$$\n",
    "\n",
    "where $\\mathcal{T}_\\triangle$ is the set of [integrated squared triangular transport maps](api-TransportMaps-Maps.html#TransportMaps.Maps.IntegratedSquaredTriangularTransportMap) (presented [here](example-gumbel-1d.html#Integrated-squared-parametrization)) of (total) order 3 and with the aforementioned sparsity pattern."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          1.229343\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000303\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         4\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:      5\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:      8\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       4\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          1.330919\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000014\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         7\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:      8\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:     14\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       7\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          1.257246\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000016\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         8\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:      9\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:     16\n",
      "2017-08-15 12:35:07 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       8\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          0.912746\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000008\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         8\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:     10\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:     17\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       8\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          0.905193\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000004\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         8\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:     10\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:     17\n",
      "2017-08-15 12:35:08 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       8\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation: Optimization terminated successfully\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   Function value:          0.928879\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   Norm of the Jacobian:    0.000013\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   Number of iterations:         9\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. function evaluations:     10\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Jacobian evaluations:     18\n",
      "2017-08-15 12:35:09 INFO: TM.MonotonicIntegratedSquaredApproximation:   N. Hessian evaluations:       9\n"
     ]
    }
   ],
   "source": [
    "rho = DIST.StandardNormalDistribution(6)\n",
    "push_L_pi = DIST.PushForwardTransportMapDistribution(L, pi)\n",
    "push_SL_pi = DIST.PushForwardTransportMapDistribution(\n",
    "    S, push_L_pi)\n",
    "qtype = 1      # Monte-Carlo quadratures from pi\n",
    "qparams = 5000 # Number of MC points\n",
    "reg = {'type': 'L2',\n",
    "       'alpha': 1e-2} # L2 regularization\n",
    "tol = 1e-3     # Optimization tolerance\n",
    "ders = 2       # Use gradient and Hessian \n",
    "log = push_SL_pi.minimize_kl_divergence(\n",
    "    rho, qtype=qtype, qparams=qparams, regularization=reg,\n",
    "    tol=tol, ders=ders)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the solutioon of this variational problem **does not require** any evaluation of the density $\\pi$, but only evaluations of $\\rho$, which is trivial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Accuracy of the approximation\n",
    "\n",
    "In general the accuracy of this approximation cannot be evaluated as with the diagnostics presented for the direct approach, due to the un-availability of the density $\\pi$.\n",
    "\n",
    "For the sake of the presentation, though, we will compare the approximation $L^\\sharp S^\\sharp \\rho$ against the actual density $\\pi$, to check its accouracy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "with open(\"aux/inv-stocvol/Distribution.dill\", 'rb') as in_stream:\n",
    "    pi_star = dill.load(in_stream)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Variance diagnostic\n",
    "\n",
    "We compute the quantity $\\mathbb{V}{\\rm ar}_\\pi \\left( \\log \\frac{L^\\sharp S^\\sharp \\rho}{\\pi} \\right)$ ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Variance diagnostic: 4.474835e-01\n"
     ]
    }
   ],
   "source": [
    "import TransportMaps.Diagnostics as DIAG\n",
    "SL = MAPS.CompositeTransportMap(S, L)\n",
    "pull_SL_rho = DIST.PullBackTransportMapDistribution(SL, rho)\n",
    "var = DIAG.variance_approx_kl(pull_SL_rho, pi_star, x=x, w=w)\n",
    "print(\"Variance diagnostic: %e\" % var)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### PushForward conditionals\n",
    "\n",
    "We can plot slices of $S_\\sharp L_\\sharp \\nu_\\pi \\approx \\nu_\\rho \\sim \\mathcal{N}(0,{\\bf I})$ ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYAAAAGCCAYAAADpDxS0AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3Xl8Vdd97/3P0oQmjiQQiEGAQQKMBXYcYysRCBsbW4ld\ncl3ahKR+0qYNeeVpk9p1b+8lkwdwm5gnTd04t31ub5I+TRPfhqctzQ3XJCRgbEtWIhs7HphBgDGD\nJGRAIxrPun/sM0pHE+icfYbv+/XSSzpn7yPWQtLvu/daa+9jrLWIiEjqSXO7ASIi4g4FgIhIilIA\niIikKAWAiEiKUgCIiKQoBYCISIpSAIiIpCgFgIhIioqbADDGfNUY899DHhcZY/qNMdlutktEJFnF\nTQAAK4A3Qx5/ADhqre1xqT0iIkkt3gPgLZfaIiKS9OIiAIwxWUAZ8HbI07cQHggiIjKJ4iIAgGXA\nOWttN4AxxgB3oTMAEZGoiZcAuBmYaYwpM8bkAE8BC4DTrrZKRCSJxUsArAB2Ay8CJ4AO4CzwVRfb\nJCKS1Ew8vB+AMeZnwPestf/udltERFJFPJ0BHHa7ESIiqcT1MwBjTBHQDORZa/tdbYyISApxPQBE\nRMQd8TIEJCIiMaYAEBFJURkT2XnTpk22tLQ0Wm2JuS1btnzfWrvJ7XaIiLhhQgFQWlrKk08+GaWm\nxN6WLVvOut0GERG3aAhIRCRFKQBERFKUAkBEJEXFdQD09A/y1ntX3G6GiEhSitsA6B0Y5LM/eI0H\n//4VLrRddbs5IiJJJy4DwOu1/Pn//xavnHgfa2H/6ctuN0lEJOnEZQB842eHef7tC/yXmqXkZKbz\n+rsKABGRyRZ3AdA34OUHv3qXDbfO5Qtry7llXoECQEQkCuIuAI41d9A34OXuZTMBWLlgGocutNPV\nO+Byy0REkkvcBcA759oAuHluIQC3LShi0Gt566xWA4mITKa4C4C3z16hICeTedNyAPjg/CIAXtdE\nsIjIpIrDAGjj5tICjDEAFORmsnhmPq+fUQCIiEymuAqAnv5BjjZ1sGJuQdjzK28o4o13L+P16s1r\nREQmS1wFwJGmDga8lptLwwPgg/OLaO8Z4MTFTpdaJiKSfOIqAN7xTfSuKC0Me37lDdMAXRAmIjKZ\n4ioA3j7bxvS8LOYUZIc9f8P0XApyMgMrhERE5PrFVQC8cy58AtjPGEPZjDxOtWoISERkssRNAFzt\nG+RYc8ew4R+/hcX5nGrtinGrRESSV9wEwKELbXgt3DxkBZDfohl5NLf36opgEZFJEjcB8NZ7zvj+\nitLIAbCwOA9AZwEiIpMkbgLgnXNtlHimUOLJjrh90QwFgIjIZIqbAHj77BVWzI08/g9ww3QFgIjI\nZIqLAOjo6edka9ewC8BCZWemM7cwRwEgIjJJ4iIADpxrx1pGDQBw5gFO6mpgEZFJERcB8Lb/CuAR\nVgD5LSzO42RrF9bqnkAiItcrPgLgXBtzC3OYnj9l1P0WFufR0TPA+119MWqZiEjyiosAeOdsG7fM\nG/3oH7QSSERkMrkeAJe7+jhzqXvUFUB+i4rzATh1UQEgInK9XA+AwFtAjjEBDDC3KIfMdMNJnQGI\niFy3uAmA5WNMAAOkpxkWTNdN4UREJoPrAfDWe1dYWJxHQU7muPZfWJynOQARkUngagAMei1vnLk8\nruEfv0Uz8jjd2s3AoDeKLRMRSX6uBsAbZy7T2tnHPctKxv2am2Z76Bv0cqSpI4otExFJfq4GwM8P\nNJGVnsbapTPG/ZrbfW8P+drpS9FqlohISnAtAKy1/PxAE6sXFzM1e3zj/wBzCnOYW5ij9wcWEblO\nrgXAwfPtnLtylY9UzJrwa2+/oYhXT1/SLSFERK6DawHw8wNNpBlYd9P4x//9bl84jYsdvZy51B2F\nlomIpAbXAuBnBy5QuXA60/KyJvxa/zzAq6c0DyAicq1cCYDjzR00XuziI8snPvwDUD4jn4KcTM0D\niIhcB1cC4G9+eYwpGWl89BoDIC3NcPsNRVoJJCJyHWIeAHsONfOzA008fM9iZo7w/r/jsfKGaZxs\n7aK1s3cSWycikjpiGgBdvQM88dODLCnJ53PVi67re/nnATQMJCJybWIWAF6v5a92Hebclat8/bdX\nkJVxff/0irkFTJ2Swf/7UiM9/YOT1EoRkdQRkwBo6ejhD/6/V/mfDWf4o1ULWek7er8eWRlpfPPj\nN/PWe1f48o53dE2AiMgEZUTrGw8MevnNe1fYc7iZf9t/ls7eAb7+2yv41B3zJu3f+Mjy2fzne5fw\nrV8eY15RDp+/s4y8KVHrUtQZY/4eaLLWbnW7LSKS/CZcLX9Qf5rDF9oBsBYsFmuhZ8BL+9V+rlzt\np6W9h5aOXga9low0w4fLpvPYb93EkpKpk96BL95dzvGWTp594QT//aWTrLyhiFmebNLSDGkGDAYA\n43wib0oGj/3WTZPejkmyGvgztxshIqlhwgHw9tk26k5cDDw2GIyBKRlpFORk4snJZPHMYmYXZHPj\nLA/VS4rxTOBePxNljOGZjR9g4+3zeOnYRV450cp7l7sZHLR4faNCluDwUFHuxC88iyZjTBrwF8Cf\nAAuAHxtjnrbW/o27LRORZDfhAPjWJ26JRjuuS3qaYVV5MavKi91uyrX4MnAf8DDwTeDTwK+MMf9q\nrX3P1ZaJSFIzE5k8NcZ8DzgbvebEXKm1dpNb/7gxZirQDNwEPABUW2s/aYw5DXzOWvtLt9omIslv\nQmcAbhbLJHU3cNRae9oYcwvwG9+QUBHQ4m7TRCTZuf6ewCluFuC/n8UHgN8Aa4A24G23GiUiqSFx\n10wmhyPAbcaYRcBynDD4J+C/Wl3YICJRNqE5AJlcxhgD/DXwR0ABcBz4S2vtD11tmIikBAVAHDDG\nbAT+0Fr7EbfbIiKpQ3MA8WEpcNTtRohIalEAxIelwDG3GyEiqUVDQCIiKUpnACIiKUoBICKSoiZ0\nHcCmTZtsaWlptNoSc1u2bPn+eK5uTtV+i0hym1AAlJaW8uSTT0apKbG3ZcuWcd3XKFX7LSLJTUNA\nIiIpSgEgIpKiFAAiIilKARBH/vlXp/nHulNuN0NEUoTuBhonGi92snXnITLSDb+7sjSqb6MpIgI6\nA4gb39h1mDRj6On3suvtC243R0RSgAIgDtQdb2XP4RYevXcJ5TPz+dfXtUpTRKJPAeCyQa/lL58/\nxLxpOfzhqhv4+G2lvP7uZU5e7HS7aSKS5BQALtt/+hJHmjr4z/cuJTsznd++dS7paYZ/01mAiESZ\nAsBlhy+0A/DhsukAzPRkc+eSGex44xyDXt2pVUSiRwHgsiNNHRTlZjJz6pTAc7/zwVKa2nvYf/rS\nKK8UEbk+ri8DNWb7mPtYuzEGLXHH4aYOls324Lw9sONDi6YB8PbZNioXTXeraSKS5FwLgMiFf8OQ\nxzuG7ZtMYTDotRxr6uBTd8wPe356/hTmFGTzzrk2l1omIqkg5gEwvPAPLfojbQuGQbKEwJlL3Vzt\nH+TG2VOHbVs+t4ADCgARiaKYBkB48fcV95UhV7yuH/KCnSFf7/eHwY6kCYEjvgngZbM8w7atmFvA\nLw4109HTz1RdFSwiURCzABix+PuLfk2EF9UAu/0PfEVw/waSJQQON3WQZmBxSf6wbcvnFgBw8Hw7\nH9I8gIhEgQtzABuCR/3rCRT+ssqDgT2qqKeeKudBJTQ2VDhf7wz5Hr4hoUR25EI7C4vzyM5MH7bN\nHwAHzrUpAEQkKtxbBRQy3FNWeZAq6gGopjbscy3VUIkTBDUV8MVM2N8PJP58wJGmDlaUFkTcNmPq\nFGZ5sjUPICJRE5MACA7/DJnwrQkW/2pqAyFQcaIRgIPlZYGzAX8QNK6vADIDQ0GJqrN3gDOXuvnE\nypHfa3j5XI9WAolI1MT+DGDl6BOaFScaA+P+FbsbnSGi8uD2xhrfcND+6DQvVo42dQBwY4QJYL/l\ncwvYe6SFzt4B8qe4fsmGiCSZGF8JvCMwfDNUFfXB4v9CyMdmqPi7xsBZQmCuwBck47mQLB4daXJW\nAEVaAuq3Ym4B1sKh8+2xapaIpJCYBMBEx+n794Z/8IJzZlBFvTNMFFgxNNo1BPHtyIUOpmZnMLcw\nZ8R9QieCRUQmmwv3AvKdBewEdgdX+ARW/Qzdu80XArsjbk5YR5s6WFoyNewWEEOVeLKZMXWKAkBE\noiJmAWDtxpAzgfAQ8Bf/g+VlYa/ZEVr3fGcB/tVBicxay9HmDpbMGnn4x+/muQW8rQAQkSiI+RlA\npBBobKigluoRzwKSTUtHL21X+1laMnYA3DKvkMaLnbT3RJ47ERG5Vq7cDnpYCHwRftjwOWqp5uAX\nysj8qwgvutv5VEv1CN8rcRxrdlYARboCeKhb5hViLbxzVmcBIjK5XHs/gEghUO+b5qUGMu+BDQXO\nR+Y9zp6BIaLAfEBiXgfgXwI6rjMA34Vib753JaptEpHU4+obwgTnBZwQaNxawTY2893yT8M2p/Bn\n3oNz9O9b+RMYJhphOWkiON7cyfS8LKbnTxlz38LcLBYW5/GWAkBEJllcvCNYIAR2MiwE/MX/YHkZ\n29jsrBraOcY3jHNHmztYMo6jf79bSgt466wCQEQmV9xcXmrtRt9FXRtppIJtj2+GcqgqD94Kop4q\nZ/hnfz+wIyHH/621HG/u4HdvG/kWEEPdMq+Qn7x5nqa2HmYVZEexdSKSSuImACByCITeGTR49J+Y\nY/8A565cpatvcFxLQP0+MK8QcOYBPlIwK1pNE5EUE1cBELC/H8ikkYrgvX924xR/39h/Ih79gzP+\nD+ObAPZbNttDZrpxAmC5AkBEJkfcBUDgLGD/BgJvAhM25p+YQz9+RwNLQMcfANmZ6Syb7dFEsIhM\nqrgLABgSAgl+18+hjjV3MMuTTUHOxN7m8ZbSQv7jN+cY9FrS00a+fYSIyHjFZQBA6HzA8OcT2bHm\njnFdADbULfMK+eGv3+VYcwfLZo98C2kRkfGK2wCAxC/2Qw16LSdaOvm/KhdM+LVrFheTZuBnB5oU\nACIyKeLiOoBUcaq1i55+74SuAfCb6cnmQ4ums/Ot81hro9A6EUk1CoAY+uWhZgCqyq/tTd4/dssc\nTrV26W0iRWRSKABiaNc7F7hlXiGlRbnX9PqPLp9NZrrhp2+en+SWiUgqUgDEyJn3u3nnXBsPrLj2\ndfwFuZncuWQmO98+z6BXw0Aicn0UADGy68AFwDmKvx4f+8Acmtt7efXUpcloloikMAVAjOx65wK3\nlBYwb9q1Df/4rVs2k9ysdH7063c1GSwi10UBEAPvXerm7bNt3L/i+o7+AXKzMthUvYjn37nAd2tP\nTkLrYsMY8xljjB3pw+32iaSiuL4OIBlYa/mXV88ATEoAAPzZPYtpvNjJ13cdYV5RLh+dpO8bZYeA\nb4c8TgM+D2QBr7rSIpEUpwAYp/5B77gnXge9lr4BL+euXOXruw5T3/g+d98487qHf/zS0gzf+vgt\nXLhylUd+/Cb/683z1CwvYfmcAqZmZ5I7JZ10Y0gzBhNy14gpGWkY485tJKy1rxJS6I0xT+EU/0vA\nJ1xplEiKUwCM09/uOcbf7Wuc8OsKcjJ56j9V8Kk75k9qe7Iz0/neH9zO3+45xs8PNPHzg01jvmb/\n19ZRPI53IYs2Y0wN8BXAAr9vrX3X5SaJpCQFwDitWTyD/Cnju4FbmoGsjDRys9K596ZZTMvLikqb\npuVlsfU/LefJ9RW8fa6N9y5109EzQFfvAF5rGXrCkpuVHpV2TIQxphT4Ec4Q0NPW2udDtm0DqoDT\nwB9ZaxP3fT9FEoACYJwqF02nctG1XcEbbWlphg/MKwy8cUy8MsZkANuBYuAl4Gsh224B5lprq40x\nXwV+F/gXVxoqkiLMRJYSGmO+B5yNXnNirtRau2msnVK135PNGPMt4M+BZuBWa+2FkG1/DHRZa//Z\nGHMb8IfW2i/Guo0iqWRCZwBuFI14kKr9nkzGmPtxij/AcWDzkAnpFsAfCG3AtNi1TiQ1aQhIYuWO\nkK9X+z5CfQHw3+e6AGd1kIhEkS4Ek5iw1j5prTUjfQD1wDrf7jXAK+61ViQ1KAAkLlhr3wSajTG1\nQAXw7y43SSTpTWgSWEREkofOAEREUpQCQEQkRSkARERSlAJARCRFTeg6gE2bNtnS0tJotSXmtmzZ\n8v3xXOSlfieH8fZbJFVMKABKS0t58skno9SU2NuyZcu4bu+gfieH8fZbJFVoCEhEJEUpAEREUpQC\nQEQkRSkAxHUDg16udPe53QyRlKMAENc99r8Oct8zL6PbkojElgJAXHW8uYPtr52hpaOXlo5et5sj\nklIUAOKqb+4+Gnjv4saLne42RiTFKADENa+/e5lfHGrm9yrnA3DyYpfLLRJJLQoAcc22nx+hOH8K\nX71/GdmZaQoAkRhTAIgr3rvUzaunLvG56oXkTclgYXE+p1o1BCQSSwoAccVrp523/F2zZAYAi2bk\ncbJVZwAisaQAEFe8euoSnuwMlpZMBaCsOI/3LnXTOzDocstEUocCQFzx6ulLrLxhGmlpBoBFM/Lx\nWjjzfrfLLRNJHQoAibnWzl5OXuzijoXTAs8tmpEHQKMmgkViRgEgMffaKWf8//YbggGwsNgJgJOa\nCBaJmQm9H4BEjzHbIz5v7cYYtyT6Xj19iezMNFbMLQg8NzU7k5lTp2gpqEgMxU0AGPPcqNutfShG\nLYmt8MK/YcjWHYHtyRQEr52+xK3zisjKCD8BXTQjj5O6GlgkZlwNgPCiv975tMwzfMfD7RjTDuxM\nmiAIFv4NsDLT+XL9kJ12+or+/v6kCYKOnn4OnW/ni3cvHrZt0Yx8dr1zwYVWiaQm1wIgWPx9Ve9P\nfYV/HeSvaw3bt/NLxbAHOLw+8LpEDgKnmPuO9rdkQg2UVR6kivrAPvVUwePQuLUC1mfCExvwnxEk\ncgi8/u5lvBYqQyaA/RYV53Glu59LXX1My8tyoXUiqSXmATDsqH+ZB9YRKPyVuQ1hhRCg/tkqGror\nfUHwEBx+DmOeS+gQYGUmrIeyx53Cv5ltVJxoBOBgeRnghMC2xzfT2FAB+EOAhA6BXzW+T0aa4db5\nhcO2lc3IB+DkxU6m5Q0PCBGZXC4OAfmO/EOK/6O5z1BJAw+8sjdsz9ZV+Tyb+zDPPP2oEwKH1wM7\nY97iyRA4+vcV/81s43Mnfgi7gRecfSrudoKAL8BmtrGtcjONVMDOTNjf71bTr5u1ll8caubDZdPJ\nzRr+q+dfCnryYhcrb1AAiESbu5PAvvH+YcX/KWdz+0vO5+I7O9n62NepXNXAJ5/+MZ0Uw3fWJ9xZ\nQKD4b8kML/6boX8v7Ghz9tuwFzLvgQoa4Qs4Z0SV0Ljedyawf0NCngUcb+nkVGsXf7R6YcTtcwtz\nSE8znLmki8FEYiGm1wEMG/cH5wzAp5IGeNn52l/8h6rMbXC+WOYB1o+5eiheBCZ9Vzpj/lXUDxvq\nCtUfchJUTW2UWxcbuw80AXDfTSURt2ekpzG7IJuzlxUAIrHg3hmAf+w/RAOVPLDGqXxha4HudIaB\nGqikobsyZk2cfL6hn8qDwad2O58y73GO/P1fA1DjzAPUUu2bByChh4B2H2ri1vmFlHiyR9yntCiH\ns5evxrBVIqkr7q4Ebl3lTARyZ8jHGiccElVg6Md39A9Djurvdj5l3uMr/ncD28Ing4GQaY8d0W/0\nJDt7uZsD59qpqZg16n7zinJ5T2cAIjERszOAiMM/IQJFDqj8ckPYtgYqaaCSZ7ofpXNPsfPkYee6\ngHgXVvx9XQ8b+qnBOQu4m7DnDpaXhR/97w7/vok2/v+Lg80AYwZAaVEuze299A4MMiUjPRZNE0lZ\n7gwBRbrYi/AQCD3ir6eKvT99wLkWYA9hxT+eJ4HD1vtDYL2/Xz1VUO6b7PUJPeqvpZofNnzOKf47\n8Q3/JN7RP8Dug00sKckP3PNnJKVFOQCcv9Iz5r4icn3cmwMYOv7fXUllboNvarQq8FznnuIhhR8S\nofgPE/nEJxACga+BWqqppyp45B//Jzqj6ujp57XTl/jju8rG3NcfAO9d6lYAiERZTAJgrOGfzj3F\n5K9rDZvgDQz1RDjqh8Qo/tZuDJ4F+JreSAWRpjNqqQ58HSj+oUKO/hNt+OeNM1fwWvjQoulj7jtv\nWi6AJoJFYiBubgbnD4FhhR8SZsgnkkAI7N8AOPf8iRQCocNfgeIfNvSTuPafvkR6muHW+UVj7lvi\nySYjzWgpqEgMxCQArH3Idxawk8BZwB6cYSD/ZwgO9/i3hwz5JFrhDzUsBHb6LuryrQhqZMjRvn/C\nN6z4J+bRPzh3/7xptof8KWP/uqWnGeYUaimoSCzE7AwgLAQO+4eCfJPBeyK8IIGP+iMJhgDABufz\nzpHuAur7PKT4J6K+AS+/OXOF36ucP+7XzJuWo6WgIjEQ0yGg8DMBQoIgkuQp/n6hR+/DwmBEweKf\niEf/B8630Tvg5Y4J3NuntDCXF462RLFVIgIuzAEEQwDGWt6STMV/qOAE8fiO7hOx+IMz/g9w2w1j\nj//7lRblcLGjl57+QbIzdS2ASLS4MgkcWtiH3ssnmYv+UMPOCEbZnqheO32ZG6bnMnPqyLd/GKp0\nmrMU9Ozlq5TPzI9W00RSnuurgFKp4I8mGYr9UF6vZf/pS6xbFvnmbyOZV+RfCtqtABCJori7F5Ak\nj5OtnVzu7uf2Cd7bv7RI1wKIxIICQKKm4ZQz/r9yAuP/ADOnTiErPU0BIBJlCgCJmp++eZ6FxXkT\nvqVDWpphbpGWgopEmwJAouLM+900nLrE795WijFmwq/X+wKIRJ8CQKLi315/D2NgwwfnXtPrF0zP\n5URzBx09iX0bDJF4pgCQSef1Wv79jXOsLi9mdkHONX2Pj982j66+Qf7l1TOT3DoR8VMAyKT71cn3\nOXflKh9fOe+av8ct8wpZVT6d79WeondgcBJbJyJ+CgCZdP+6/z2mZmeM+Obv4/Und5XT0tHLjjfO\nTVLLYs8Y8xljjB3pw+32SWpz/UIwSR5Xuvv4xq4j/OTN8/zBhxdc920cqsqmc3NpAf/wUiOfWDmP\n9LSJTybHgUPAt0MepwGfB7KAV11pkYiPAkBGdaKlgz2HnRuz2ZDjVYvF67UMeqG9p5+LHb3UN7Zy\nubufz9+5iEfXLbnuf9sYwx/fWcYfP/cGH/nbl7m5tJD503LJSDcYAwYTsu/I3+e3bp4duLgs1qy1\nrxJS6I0xT+EU/0vAJ1xplIiPAkBGdfhCB0//7Mio++RkpjPTM4WKOQX8l5qlLJ9bMGn/fk3FLL72\nwDJeOdHKS8cu0trZO+HvsWJugWsBEMoYUwN8BbDA71tr33W5SZLiFAAyqo8un8XhrR8JPA490k4z\nhjQDGenRm0pKSzNsql7EpupFAAwMevFa8IacjtgxRtKzMtyf6jLGlAI/whkCetpa+7zv+QLgl8BN\nwIestQfca6WkGgWAjCojPY2MOLojczTDJlqMMRnAdqAYeAn4WsjmbuAB4JsuNE1SnLFjHT6F7mzM\n94Cz0WtOzJVaazeNtZP6nTTG1e/JZoz5FvDnQDNwq7X2QoR9/gn4a50BSCxNKABEZGKMMfcDz/se\n1gGvD9llq7X2kgJA3KAhIJHouiPk69W+j1B/i7MiSCTmEm9AVSSBWGuftNaaUT5Ou91GSV0KABGX\nGWN2AfcB3zXGfMbl5kgK0RyAiEiK0hmAiEiKUgCIiKQoBYCISIpSAIiIpKgJXQewadMmW1paGq22\nxNyWLVu+P54rQ9Xv5KB+iwxhrR33xxNPPGGTCfCkVb/Vb/VbHyn6oSEgEZEUpQAQEUlRCgARkRSl\nABCJsf/99nn2n9b938R9CgCRGLLW8tX/OMAze4653RQRBYBILDW399J2tZ8D59qxVvfhEncpAERi\n6GhzBwBtV/s5e/mqy62RVKcAEImho03tga8Pnm9zsSUiCgCRmDrS1MG0vCzS0wwHzrWP/QKRKNJb\nQorE0NGmDirmeLjY0cs753QGIO7SGYBIjAwMejne0smNs6ayfG4BB861aSJYXKUAEImR0+930zfg\nZeksDyvmFvB+Vx/N7b1uN0tSmIaARGLkaJOzAujGWVPpHRgE4MC5NmYVZLvZLElhOgMQiZGjTe2k\nGSifmc+y2R6MgQNaCSQu0hmASIwcaerghul5ZGemA1A2I58DmggWF+kMQCRGjjZ3sHTW1MDjFXML\ntBRUXKUAEImB7r4BzlzqDguAijkemtp7aO3URLC4I26HgIzZPup2azfGqCUSbeE/641A8HGy/JyP\nN3dirTMB7Ldgeh4A5y5fpTh/iltNkxQWVwEwvOhvGHPfRC8QYwUdJH4fRxLsu69/6wg+3gOwHWO2\nJ0X//fcAWlISDIBZHmf1T3N7jyttEnE9AIJFYAOs9P2hr/c9VeP7vDvkBTt9n/dvAHZEu3lR4fTZ\nF24rNwb7GyrQz36M6cff10QqhsOP7EOsA9b5+v7gQPi2n/h+LfeEnw0ksmNNHUzJSAsc9QOUeJyj\n/uYODQGJO1wLgPDCn+kUghooqzxIFfVUUxvcuRJqqaaeKhqpcJ7bH/w+iVcUg30ue9zp71D1j1fR\n2FABuzOdJ3Zu9IVBYvQ34tG9P+h8BX/1/H2soZa72AfAi6wF4OWHq6nbeW/sGhsDx1o6KZuRT3qa\nCTw3PX8K6WmG5jadAYg7Yh4AYYUfnEL434KFfzPbqDjR6GzzH/nXQFV5PfVUse3xzU5h3JmZcGcB\ngeK/JRNq4NOV36WaWqqoD/bZ52B5GfWVVWyr9PUXAKfPiRICsDFY+CMU/bXtdWTsIvBzvremjoH7\n4QnPY9R9fy0scP0EddIcb+6gcuG0sOfS0wwz8qdoCEhc4+5fmP/IH8KL/27ghfBdK2ikvrwq5k2c\nLGFH/iHF/3MnfhgMOn+f73b6S7nz/0IlzpnPzvDvlxghEOQv/lvanwor/Ozyfa4Z4YUJrr2nnwtt\nPSwJmQD2K/FM0RCQuCamATDSsI+/GFacaITN0L83/HWZOPsFhkpC5gQSrQiy3jnbCS3+/V91Nu3w\nXRO0YS9VF2mbAAAfNklEQVRkvgAV2xrZXL6NbWx2AmA9vqGvxDrz4cEBVs/fx+NsDR71PwLbW8J3\n27gb9n1yNS9THZwHSALHmzsBWDJzeADM9GRz5v3uWDdJBIij6wAijYPvaAsWxWH2BydG413Y0f9Q\nLwx/KmB3hOcifY9EE6lfIerOrPWd7STeWU4kxyOsAPKb5cmmuUNDQOKOuDjMqqeKeqqg3DnqzfQV\niMCffg18t/zT1FJN41b/UMiOhC8OB8vLnP5udh4HenM3UONs38Zm5/9mN06/fcEX7323dqMTfHs2\nOkfzDzuTvGupY+Dbzi/eRv/Qz/3Op4Fvw1Yed/bfk4BndyM41txJTmY6pUU5w7aVeKZwpbufnv7B\nwC0iRGLFvQDY349vcIdGKthWudlZ/VNeS1V5+NmAvwgGiv/+xDkyHGmdfy3Vzhe+0At1sLyMeqqC\nK58C/U6cs54wO6HuwbUwH/Z5VrO23RcCIWP+gcnfZ++FR8BZ/pkYP+OxHGvuoHxmPmkhK4D8Zvqu\nBWhp72X+9NxYN01SnEsBsIOwi7x2+yY5K52HgeKIc3bgLIckUAQTpfiPpLEh2Fdg2OR2oPAP6Xei\nXQsw9Cyg7sG1vDi/FjzO9rX317HPsxpwzg6+fubxpBr68TvW3MHqxcURtwUuBuvoUQBIzMU0AAIF\nwS/kLACcEAis8/cLK4BgbYKPgftW8vj7Wl85fGVToPD790/A4h9uu3MdAxl8/cHHeXl+NWuo5UWP\nb90/1c64/2czfFcAJ4+27n5aOnpZGmH8H6BEVwOLi1ycA/CdBfgKO2SGLXMMCGyP/3HvMe3vdyZx\nQ/rZuLti+H4hVwE7Erf4B88C/M9kUMe9zpCQ308ynD77bv+QiP0cybGWkSeAIXg1cJMuBhMXxDwA\nws8C/OPZoUEwVOIWv3AhgbdyhLDzG1L4IbH7HzYU5Lczw1nW6v9/SMLiD87wD8DikvyI2wtyMsnK\nSKNF1wKIC1w5A/D/kQ8PgtH3T3xDz3pG28+RLH2PGAJhwz3JV/zBuQYgLyuduYXDVwABGGOY5cnW\nGYC4wtVloMPmBCJsT3TD+zi+VTzJ0PehRvt5J2N/wXkf4PKSqRgzfAWQX4lHt4MQd7h+HUCy/uGH\nGn7GM/a+ySpSCCRrn7v7BnjjzGU+dcf8Ufeb6cnm0Hm9M5jEnusBkEqStdBNVKr8P7x09CK9A15q\nKmaNut8sTzYvHG7BWjvqmYLIZIubW0GIJJvdB5soys3k9huKRt2vxDOFq/2DdPQOjLqfyGRTAIhE\nQf+gl71HWrhnWQkZ6aP/mZUErgbWPIDElgJAJAp+ffJ9OnoGxhz+gWAANLVpKajElgJAJAp2H2wi\nNyud6hFuARFKVwOLWxQAIpPM67X84mAzdy6ZMa47fAbfG1gBILGlABCZZK80ttLS0Tuu4R+A3KwM\npmZn8N6lq1FumUg4BYDIJLrc1cd//be3WTA9l/sqSsb9ujVLZvDTN89xqasviq0TCacAEJkk1lr+\n4l/f4v3OPv7u9z5Ibtb4L7P5s3sW090/yD+81Dj2ziKTRAEgMgl6+gf55u6j7D3SwlcfWMbyuQUT\nev3ikqk8+IG5/OBXp2lJkrkAY8xnjDF2pA+32ye6Eljkmlhreb+rj1OtXew/fZl/fOUUFzt6efAD\nc/j9Dy+4pu/5yD2L+elb5/m7F07w5McqkuGq4EPAt0MepwGfB7KAV11pkYRRAIhEcPJiJ5/67q8D\nj60FrwWvtfT0D9LTP4g35Bh2dXkx3/nUrVQunHbNhfuG4jw+flspP/jVu/yo4Yxzq+gxLiIbzSPr\nFo95H6Josta+SkihN8Y8hVP8LwGfcKtdEqQAEIkgb0oGa5fODHsuLc2QZiA7I52crHSm5WVxQ3Ee\n5TPymTdtct7O8bHfuokbZ02ltbOPy919DAxe+0hJpDehd4sxpgb4CmCB37fWvutykwQFgEhEJZ5s\nnv6dm2P+7+ZNyeAzqxbG/N+NJmNMKfAjnCGgp621z/uevwNniKgfOIcTDGO9WYZMImPt+I8wjDHf\nA85GrzkxV2qt3TTWTup30lC/Y8wYkwG8BFT5Pt9jrR30bZsNXLHWXjXGfAN43Vr7b260M1VNKABE\nRCbCGPMt4M+BZuBWa+2FEfbbArxlrR3fOybJpFAAiEhUGGPuB573PawDXh+yy1Zr7SVjzALgx8Aa\nDQHFluYARCRa7gj5erXvI9TfGmMGgB8Cn1Hxjz2dAYiIK3zzAz8FvmWt3et2e1KRrgQWEbd8CqgE\nHjPGvGiMSY33Co0jOgMQEUlROgMQEUlRCgARkRSlABARSVETWga6adMmW1paGq22xNyWLVu+P54r\nJNXv5KB+jy5V+53SrLXj/njiiSdsMgGetOq3+q1+p3S/U/lDQ0AiIilKASAikqIUACIiKUoBICKS\nohQAIhITf/X8IT77T6+53QwJobuBikhMvHCkhZOtXbR191OQm+l2cwSdAYhIDHT1DnCytQtr4den\n3ne7OeKjABCRqDvS1I7/vpP1J1rdbYwEKABEJOoOnm8HYElJPvWNOgOIFwoAEYm6g+faKcrNZMMH\nSzne0klLe4/bTRIUACISAwcvtFExp4BVZcUA/OqkzgLigQJARKKqf9DLsaZOKuZ4uGmOh4KcTF7R\nPEBcUACISFQdb+6kb9DLTXM8pKcZPrRoGq+ceB+rdyN0nQJARKLq4Pk2ACrmFACwqryYc1eu8t6l\nq242S1AAiEiUHTzfTk5mOguL8wBYuWAaAG+fu+JmswQFgIhE2aHz7SybPZX0NAPA3KIcAJratBLI\nbQoAEYkar9dy6EI7y+cWBJ7zZGeQm5XO+SsKALfF3b2AjNk+rv2s3RjllsTOaH1Opn5GMlLfk7/f\nzwW+tvYhF1sSXWcuddPZO8BNsz2B54wxzC7IpqldcwBui5sACBaCkD/8dUN22uP/YjvGbE/oIhFe\n+DaMsNeOsP0Sub9DDft5ryPk50vC/3xHEiz864c9l4xBcLylE4Cls6aGPT+7IIcLGgJynesBELEQ\n+P82HhwI7viTDOf5ncCejSRyCAT77Cv8K313RvT3e6d/z42wv9/39Y6E7a9feOhF+HmH9n/PxoTv\nb6iwwr/ME77xsPOLbcxzSRcCx5o7ACifmR/2/KyCbOqO61oAt7kWACMW/gcHeGz+EwBU8zIAtayB\nh2Efd1HHvc7+vhBINIF+r9zo9LcGyioPAlBFPQD1j1cB0NhQAbt94fDEhsDrE6UoDi/4G4Nndb6f\n9er5+1hDLQAvUw3g/Iz3kBScwr/e+fhTD6yD/HXhha/zS8XwHf/RTXI50dLJ7IJspmaH3/55TkE2\nLR09DAx6yUjXVKRbXAmAsOI/pPBX8zL3/rjO2bzb+XRvTR0D90O152W2PkxYCCReQdzgHPH/N6fw\nb2ZboPBXnGgE4GB5GQD1lVVsq9zsBAGZTn3YvyHw/xfP/R72M4bAzxngK/O3chf7WNteR8YuZ/PA\n/bDPszr4M34ksQIv1LAj/qedwv9o7jNU0gBAA5UAPPP0o3TuKYbDLjU2io63dAw7+geYVZCD10JL\nRy9zCnNcaJmACwEwruK/G9gV/roMYO39ddR6XqTuwbWw0/XRqwkJHvlnwvrw4l/xd07h5wXnU8Xd\njVADlPvOCiqhkYoEPEAMKf7fdwr/6vn7eJytwcIf8rP2/4xf9NT6Qj5Rz/JGLv4P8yzF3+iENVC5\nqoEGKqnMbWDvugfgMEk1DOT1Wk60dPJQ5YJh22YXZANwoa1HAeCimFbRiBO9vmGAal5mbXt48d/e\nMmzvBOcb869xCnug+PsKf/9e53PgZLkcqqmlnqpYN3RyrScw1HMX+4aFfODnvBu4PzgUlNh8xX/d\n8OLfvhU8d0LxY52wKvxVyVL8Ac5duUpPv5fFEc4AZhc6AaBrAdzl3mF0yOTfWl4MHhHuChaEgF1A\njTM8sI+7nAlhABJseCDk6L+aWmfI54Vg4d/hXDHPhr2QebczJFRfnuDFH+DBAdZQy5b2p8KO+of9\nnJNA6PJO1jkflbkNYcV/Zw+wGx660zkLSFbHW5wJ4MUlEQLA4xz1X2jTUlA3xc04yj7Pau6lDu6H\njUOGf7jfGR+uZQ11Z9b6Vom40cprExj7j+Ru54i/fy9s8F0rk3mP89k/FxBJQgWfT+BnHMHGmQRC\nHvANdyVYwI+D504Cc1vgzAM0dFcm1O/zeB1vdpaAls+YOmybJyeDnMx0LQV1WVwEwD7ucr74pDMG\nnFETvv2Xn1xNLWt46swW+GyG748l8YvDwfIyKnDG/zPvDtlQ42zbxmbqqXImgb+Ib0noDjeaeo22\n4x/Ae5lq7mKf87Tv57txF3A/ged++cnVbOVx6p5NklVAewjMgTRQyQNrnFO9wCDPGud5ZwK4PamG\nf8C5BmDm1CkR3wDeGMPswmwNAbkspgFg7cbgPEDgDzzDmdSd73voAT4ZfE0ta5zln8/eG3Lkn/jF\nv9Y/zl3u+whRTxW1VI9Y/BOh74Gf9R6cIbuHnecH7scZBqohEAT+1T8vsjZ4hkdi9HMiWlflU0xn\n4PHzq+7hme5HkyPsIjje3BFx+MdvdkE25zUE5CqXzgCCR4bOH3swBAJnAz51Z9Y6BSSJin9jQwW+\nFYDUUk21bx28PxQChX83vqWfiVX8h9kJdQ+uZet8wLM1LOABXmQtL1Pt/KxDzvAScfrf2od88wA7\ngYdgD+zlAfgYPMozsCq4+ucZfMs/9xDcP0lYazne0sknVs4bcZ9ZnhzqG3UxmJtiHgDBs4DtzsVc\n6wiGgH99/4MDwYnesPH+BC/++/vxr/FppMJZ2omz3h98wQDBwh94TWIW/+BZwEb4bAZ133dCYNiF\nX0kW8oEQONyOc0oLDesqeSb3UcBZAVZPFXt/+gB8Cd9+yeV8Ww/dfYOjngHMKcymuV0Xg7nJ5TkA\nX3EYyr/GP6TwQ+IVwHA7gA1hIeDXuLsi+CCs8Ptfl7h9jxQCdawN7jAs6BNv3f/Idvpu8+Ch80vF\nzlp/fGcE4PT3cDuwM/nG/323gFg8c/gEsN+sgmy8Fi529jK7QNcCuMGVAAibC/D/wUcKgpBikKgF\nMNzIIQCEFH3/vsnSbwiE/WdD7unkl1RB7wgbCvKFwDDfSc7iD84tIICI1wD4zSnwLwXtUQC4xLUz\nAP8f+bAgGGG/RBYeeL5VPPuHLgsdvronGfoOEYb9hk16JlvQO4aFwLBbPSTcpd3jdvhCB8X5WRTl\nZY24zyz/1cBXeoKLQCSmXF8GOjwIwp9PFuEhAKMt50y2vsOQEBhhezIKnxSGobd8Tcajf2stdScu\ncsfCaaPuF7wdhFYCucX1APBL1gIQamgfE/VGZ9dqeAgGn09mwRCA0KP+ZCz+4Bz9N7f3ctfSmaPu\nV5CTSU5muq4FcFHcBEAqSvbCF0kq9hmSt9hHsu+oc4+Pu5bMGHU//zuD6Wpg92jtlYhMqhePtrB8\nroeZnuwx951dmM27l7pi0CqJRAEgIpOmrbuf19+9zNoxhn/8Vi6YxsHz7Vzu6otyyyQSBYCITJqX\nj1/Eaxlz/N/vrqUzsNZ5ncSeAkBEJs2+oy0U5mbygXmF49r/5tJCinIzefGoAsANCgARmRRer+Xl\nYxe5c8kM0tPMuF6Tnma4c8kMXj52Ea/XRrmFMpQCQEQmxT//6jStnX3UVMya0OvuWjqT97v6eOdc\nW3QaJiNSAIjIdTve3ME3fnaEu2+cyUeXTywA1iyZgTFoGMgFCgARuS59A14e+fGb5E3J4OnfWYEx\n4xv+8ZuWl8UtpYWB6wcSnTHmM8YYO9KH2+0LpQvBROSavXHmMs/88hiHLrTzPz59GzOnjr32P5K7\nls7g23uPc6q1i4XFeZPcypg7BHw75HEa8HkgC3jVlRaNQAEgImF+fqAJcA5UrQWvhUFr6R/w0jvg\n5crVPt67dJUjTe385swVCnIyeey3buK+CY79h3pgxWz+fl8j6/7mJWoqSrhryUymZKaRlZ5G8IRi\nfGcWpUU5LJ9bcM1tuV7W2lcJKfTGmKdwiv8l4BNutSsSBYCIhPmT515nrAU50/OymDctl689sIxP\n3TGfvCnXV0oWl0zlhb+4kx/++l1+/Op77Hqn6Zq/18dvK+WbH7/lutozWYwxNcBXcBL1962177rc\npDAKABEJ8/zD1WGP09MMaQay0tOZkplG/pSM6y74kZQW5fLljy7j0XVLuNjRS9+gl74BL+CciYxX\npDehd4MxphT4Ec4Q0NPW2ud9z5cA/wH0A4PAQ9baC260UQEgImGWzY7w5jUxlJ2Zzrxpua624XoZ\nYzJw7n1eDLwEfC1kcyuw2lrrNcZ8Bvgs8JcxbyRg7ASi1RjzPeBs9JoTc6XW2k1j7aR+Jw31exSp\n2u9oMMZ8C/hzoBm4daQjfGPMnwJnrbX/Ecv2Bf79iQSAiIiMzhhzP/C872Ed8PqQXbbivAfaPwCF\nwH1uzQ0oAEREJpEx5kngiVF2WWitPe3b9xPA3dba/zsGTRtGF4KJiEwia+2T1loz0gdwPmT3NqDb\npaZqElhEJMY+YIz5a5wVQD3AH7nVEA0BiYikKA0BiYikKAWAiEiKUgCIiKQoBYCISIqa0CqgTZs2\n2dLS0mi1Jea2bNny/fFcKah+Jwf1e3Sp2m9Irr5PpN9Ya8f98cQTT9hkAjxp1W/1W/1O6X7bJOv7\nRPqtISARkRSlABARSVEKABGRFKUAEBGJotbOXl6M0ze8VwCIiETR/3j5JH/4T6/R2TvgdlOGUQCI\niETRofPtWAunLna53ZRhFAAiIlFireXwhXYAGi92utya4RQAIiJRcrGzl/e7+gAFgIhISjlyoSPw\ntQJARCSF+Id/bltQRGOL5gBERFLGkaYOZhdkc9uCIk6938WgN77egEsBICISJYcvtHPjrKmUzcij\nb8DLuctX3W5SGAWAiEgU9A14OdHSybLZHspm5APxNw+gABARiYITLZ0MeC03KgBERFLLkSZnAnjZ\nrKkU5WVRlJupABARSQVHmjrIykhjYXEeAGUz8uNuJZACQEQkCg5faGdJST4Z6U6ZLZuRz8nW+DoD\nmNBbQsaaMduHPWftRhdaIiIyMYcvdLB26YzA47KZeWzf38eV7j4Kc7NcbFlQ3AXA8KK/wfd5R9j2\nZAoCY54bdbu1D8WoJbE1Wr+Ttc8w/Hc8mX6Xx5Iqfb/S3UdrZy+LS/IDzwUngru4bYECABj6C7EB\nVvp+IdYP2XGn7/n9/cAOjNme0L88weK3HpaNUuwOt2NMO7ATSMzCGLnQj9Dvw05fjXkuIfs6kuDv\n+UbfR/i2RP5dHk14v0M/h29Ptv6fanXG+hcVRwqATm5bUORKu4ZyLQCCvxi+I/yVmU7Rr4GyyoNU\nUR+2f/3jVTQ2VMAXM4GNsL8/If9wAsXQX/yehvx1rQBU5jYA0NBdCUDnnmLY43H2+876wOsTpTCG\nhRzAMl9f1hH4HNr3vT99wOnvnofg8HMJ1deRhBXAdTj/FQ/67gv/E9+f3yMbE/J3eTSj9tvvJxnw\nyEZge9L13x8AC2fkBZ4rLcohPc1wujV+JoJdCYCw4h9S+D9d+V2qqaWKeipONIa95mB5GdsqN1P/\nWhWNWyuATNgf65ZfH6cgrncK4TrIf7qVytwGqqinkgYq8QVArhMADR+r5Jl1jzpBgAf2AIfXJ0Rh\nDPQVAv0FAkU/Ur+f/djD1H+sygmCLyV2CIxUAFfP38fjbKWWNfAw7OMu6rg3aUJgtH6v5cWwffc9\nHOw7DJ/vS2SnWrtITzPMK8oNPJeRnsYsTzYX2npcbFm4mAfAWMX/cyd+CLuBF0JedDdQDtXUUk9V\nyIYNCfNHEzzyDxb/R3Of4WGepfgV38qAl51PD6zZS+sq53SxMreBhnWVdFLsBEDI90uIwugv/hEK\n/wOv7HX28fX74S8/C+D0d10xHHanydcrUhFc/fAvWcuLPN7+dTJ2wb3U8ctPrgag7sG1sDMj7Oeb\n2MKL/2Pzn6Cal7n3x3UAgX7v4y7XWhhtJ1u7mFeUQ1ZG+ELLOYXZnLsSP7eDiGkAjFb8N7PNOerf\nDP2+urCjzfm8EaigEb4AVdTTWFPhHxJPLMseChT/H+d+kkoaKP5GJ7wE7S8Fd/PcCcV3dlL5ZadY\nkgt7ecD5ozoMzn9cAv0HrIN7PvZ8eOF/GfD12d/3Ypw+V+Y2OP1NtH6GCS/+j7OVte11ZDwC7HL2\nuJc6+CSsnr/PORJOcGHzeb7i/4v5a8P6vb0FNu6uY+DbUOt5MSn6Hcmpi12B9f+h5hTm8MaZyy60\nKLKYXQcwbMzfp6zyYPDBbufTjrZg8YdgICQ233DIuuBYf/Er4cV/Z4/z4Vf8SmdgeCTAP46egEKH\ne/z9Du27fx8gOGSUYCItXfbL2EWgCG5vAXbD2va6mLUtNjYGfnar5+8DwvsNztcZvhAcNi+QBKy1\nnGrtYmHIBLDfnMIcmtp64uauoK6vAmpsqKCqsp56qqioccb9hw3o3A0Hv1BGPVX8sOFzTlD4VgMl\nzvDP0GVN0Loqn+I7O/GX9MCAzp3AGnh+1T08w6POpPAefHMAwRVBiaaBSippGNZv8PV9DTzLw8H+\nJmA/rd04YggM3A8Zu2Gjv/jVwD7PaurOrI1dA6NuO0P/gof1+37nuX3c5ZsIT67x/+b2Xq72D4ZN\nAPvNKcyhf9DS2tlLiSfbhdaFcyEAdjD0LCAwrl8OVeX1gSDw+275p6mlOlj8n+jHf11AImrorgxM\n9FZ+uYHiNeFXB7auyqeBykDxd1YDEVb8E2L8n53AQ7DHGdcnN3iEH6nfz6+6h3qqgv0lUfo5uroz\na6md/yJ4YO2365w/uhpnLLyWNU4R3JNESyF9P7u6B51+r8UZ8vEXm4Fvw1bPV5zg82V80vQdAlf7\nLoowBDS30Cn6565cTa0AGHZktL8fyASgkQpw6iG1VFNdXhvYrZZq6vEtAd2N7xdmR+B7Jpw9wDrC\nJ7NXBb9soJJ6qsILfwIWf2sfcs58DrfDOg+de4ppWBcMPiCs3wDP8KhvKSi+/iay7bDH//uZwb6H\n73K+9IUAQC1reOrMlkQ80Ylo2N/4T5x+V3ucWX5/v7d6vuL0O9mCzyewBHSEOQCA81eu8sH57l8L\n4NIQkO8sIDQEdlc4k7tAfaVTHBsbnMeBwr8/eOSfeL80O+HwesBD55eKaXjaGRH3zwfAkPX/kLDF\n3y8QAt/x9Ztinln3KMDI/d4DfMfpbyL1NVSwEAZDoI57qXtwLavn76PW86Kz/PPM2kARjDR0krhC\n+v3gWrbOd56t9bwIECz+j/j2TZp+O05d7CI701nyOVRoAMSDmAZA+BFCSAjsx1kV5DsSalw/ZJXP\n/v7AaxKv8EewBzq/5BR5Z7XL8O3AsPH+RC2IQCDMOteN0e8EnuMIFfa7HljemeEEgX/ly04CxT8p\nfq+JEH6fzaBuvdPfugd9cx2fzUi6foc61drFDdPzSEszw7Z5sjOZOiWD81fi41qAmJ8B+H/gzi+J\nfxx/Q0iRJ8IFXol61O8IHAkHzgIARljNEzb0kXhH/UMFh4LG2+/E73M4fwhsDAz/BewJ2Z5EIp0B\nAc61DpC0/fY71drFjbOnjrh9TmFO3FwL4NoqoMhBMPq+iSwsBCCkIIYKP/JNliIY7Dsj9BuS5kwn\nRPgZb0gQBCTnfXBglBAAkrnf/YNezlzq5qMrZo24z+zCbC60pXgA+CXjL8FIwgrhCMMcyVL8hhoW\ngBG2J6PwAx0IPfJN9t/9sBCIsC0Znb18lQGvDbsJ3FBzCnN4+2zbiNtjyfUASDXJWujGw9/3hLmN\nxSRK1oI3llTr9ynfEtBI1wD4zS3M4VJXH1f7BsnJSo9V0yLSO4JJzKVa8ZfUcbTJFwDTRw6AOb5r\nAc7HwTCQAkBEZJLsO9LCstkeivJGfsOXOQXxsxRUASAiMglaO3t57d1L3HdTyaj7xdO1AAoAEZFJ\nsPdwM9bCfRWjB8CsgmyMgXNxcC2AAkBEZBL84mAzpUU53DR79Dv2ZqanUTI1W2cAIiLJoKt3gNoT\nrdx30yyMGX4F8FBz4uRaAAWAiMh1eunYRfoGvGMO//jNLsyJi9tBKABERK7TLw42UZSbycoF47vD\n58LpeZy51M2777v7BvEKABGR6/D6u5f5+cEm7r2phIz08ZXUT394AVMy0vj6Lnff+FoBICJyjd45\n28Zn/vFVZnmy+Yv7lo77dSWebL6wtpzdB5upP9EaxRaOTgEgIjIB1lpOXuzkB/Wn+fQ/NlCQm8n/\n/NyHmDnBd/j67OqFlBblsPV/H2Jg0Bul1o5O9wISEQG+9O9vc9o3Jm8tWAALXmsZtJb+QS8dPQNc\n7uqjvcd5M/ulJVP53h+sDFzcNRHZmel89f5l/PFzb7Dyr/ZQMjWbgtxM/GuIxrGYKMyiGfl8/bdX\nTOg1CgAREZxC77XBxwYwaZBh0khPM2SkGcpm5OPJzmTJrKmsWVzMglHu+TMeH1k+i22/s4J3zrXR\n3N5LR09/IHysHfPlYexEX4ACQEQEgP/nd2+J+b9pjGHj7fPZeHvM/2nn359IahhjvgecjV5zYq7U\nWrtprJ3U76Shfo8iVfsNSdf38ff7Wk4bREQk8WkVkIhIilIAiIikKAWAiEiKUgCIiKQoBYCISIpS\nAIiIpCgFgIhIilIAiIikKAWAiEiK+j+phjHDmZh5HQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f0f5db8d2e8>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "push_SL_pi_star = DIST.PushForwardTransportMapDistribution(SL, pi_star)\n",
    "fig = DIAG.plotAlignedConditionals( push_SL_pi_star, range_vec=[-5,5], \n",
    "    numPointsXax=20, fig=plt.figure(figsize=(6.5,6.5)), show_flag=False,\n",
    "    vartitles=[r\"$\\mu$\",r\"$\\phi$\",r\"${\\bf Z}_0$\",r\"${\\bf Z}_1$\",r\"${\\bf Z}_2$\",r\"${\\bf Z}_3$\"])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
